import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// Make GET Request
response = WS.sendRequest(findTestObject('ReSTWebService/GetUserDetails'))

WS.verifyResponseStatusCode(response, 200)

// Verify data from response
WS.verifyElementPropertyValue(response, 'data.first_name', 'Janet')

WS.verifyElementPropertyValue(response, 'data.last_name', 'Weaver')

// Make POST request
PostResponse = WS.sendRequest(findTestObject('ReSTWebService/PostCreateUser'))

WS.verifyResponseStatusCode(PostResponse, 201)

// Verifying response
WS.verifyElementPropertyValue(PostResponse, 'name', 'morpheus')

WS.verifyElementPropertyValue(PostResponse, 'job', 'leader')


//def PostResponse = (RequestObject)findTestObject('ReSTWebService/PostCreateUser')
//
//String body = '{    "name": "morpheus",    "job": "leader"}'
//
//try{
//	PostResponse.setBodyContent(new HttpTextBodyContent(body,"UTF-8", "application/json"))
//	}
//	catch(Exception ex){
//		println (ex.detailMessage)
//	}
//	
//WS.sendRequest(PostResponse)



