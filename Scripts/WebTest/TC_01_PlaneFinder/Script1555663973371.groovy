import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://planefinder.net')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/Page_Flight Tracker - Live Flight T/input'), 'Delhi')
WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_Flight Tracker - Live Flight T/span_Indira Gandhi Intl (DEL)'))

WebUI.click(findTestObject('Object Repository/Page_Flight Tracker - Live Flight T/div_On Time'))

WebUI.click(findTestObject('Object Repository/Page_Flight Tracker - Live Flight T/button_View in database'))

WebUI.verifyElementText(findTestObject('Page_Air Service Gabon Flight G8336/link_Aviation Database'),'Aviation Database')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Air Service Gabon Flight G8336/a_Flight Tracker'))

WebUI.delay(2)

WebUI.closeBrowser()

